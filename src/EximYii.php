<?php

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 22.08.2018
 * Time: 10:32
 */
namespace Newdir\Exim;


use Newdir\Exim\EximApi;

class EximYii extends CApplicationComponent
{

    protected $_api;

    public function __call($name, $parameters)
    {
        if (method_exists($this->_api, $name)) {
            return call_user_func_array(array($this->_api, $name), $parameters);
        }
        return parent::__call($name, $parameters);
    }

    public function init()
    {
        $this->_api = new EximApi();
    }
}