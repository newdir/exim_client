<?php

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 23.08.2018
 * Time: 17:43
 */
namespace Newdir\Exim;


use Newdir\Exim\EximMessage;
use Newdir\Exim\EximMember;
use Newdir\Exim\EximList;


class EximMemberMessage extends EximMessage
{

    private $member = null;
    private $list = null;


    public function setMember(EximMember $member)
    {
        $this->member = $member;
        $this->type = 'member_message';
        return $this;
    }


    public function setList(EximList $list)
    {
        $this->list = $list;
        $this->type = 'list_message';
        return $this;
    }


    public function getPostData()
    {
        $data = parent::getPostData();

        $data['type'] = $this->type;

        if ($this->member && $this->member instanceof EximMember) {
            $data['member'] = $this->member->getPostData();
        }

        if ($this->list && $this->list instanceof EximList) {
            $data['list'] = $this->list->getPostData();
        }

        return $data;
    }


}