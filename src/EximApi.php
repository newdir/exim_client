<?php

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 22.08.2018
 * Time: 12:17
 *
 * Класс распределяет отправку писем по серверам согласно email-отправителя
 */
namespace Newdir\Exim;


use Newdir\Exim\EximMemberMessage;
use Newdir\Exim\EximException;

class EximApi
{

    private $domain;
    private $_curl;


    private $from = ['info@newdirections.ru', 'Новые направления'];


    public function __construct($params = array())
    {
        $this->domain = (isset($params['domain'])) ? $params['domain'] : null;
        $this->from = (isset($params['from'])) ? $params['from'] : $this->from;
    }

    public function getServerName(EximMessage $message)
    {
        return ($this->domain) ? $this->domain : 'mail1.newdirections.ru';
    }

    public function newMessage($object = null)
    {
        if ($object) {

            $message = new EximMemberMessage($this);

            $class_name = get_class($object);
            $class_name = substr($class_name, strrpos($class_name, '\\') + 1);

            switch ($class_name) {

                case 'EximMember' : $message->setMember($object);
                                    break;

                case 'EximList' : $message->setList($object);
                                    break;
            }
            return $message;
        }

        return new EximMessage($this);
    }

    public function sendMessage(EximMessage $message)
    {
        $result = $this->_performRequest('POST', '/message.php', $message);
        return ($result['success'])
            ? ['success' => true, 'message' => $message->getId()]
            : ['success' => false, 'message' => $result['message']];
    }

    public function createMailingList($list)
    {
        $response = $this->_performRequest('POST', '/list.php', $list);
        return MailgunList::load($response['list']);
    }

    protected function _getCurl()
    {
        if (!$this->_curl) {
            $this->_curl = curl_init();
            curl_setopt($this->_curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($this->_curl, CURLOPT_TIMEOUT, 10);
        }
        return $this->_curl;
    }

    /**
     * @param string $method
     * @param string $url
     * @param MailgunObject $object
     * @param array $params
     * @return array
     * @throws MailgunException
     */
    protected function _performRequest($method, $url, EximObject $object = null, $params = array())
    {
        $url = 'http://' . $this->domain . $url;

        $curl = $this->_getCurl();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);

        if ($object !== null) {
            $params = array_merge($object->getPostData(), $params);
        }

        $json_encode_params = '';

        if (!empty($params)) {
            $json_encode_params = json_encode($params);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $json_encode_params);
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json_encode_params)
        ));

        $count = 0;
        $max_tries = 5;
        $success = true;
        do {
            $response = curl_exec($curl);
            sleep($count * $count);
            $count++;
            if($count >= $max_tries) {
                $success = false;
                break;
            }
        }
        while(in_array(curl_errno($curl), [CURLE_COULDNT_CONNECT, CURLE_OPERATION_TIMEOUTED]));


        if ($success == false || $response === false || ! json_decode($response, true)) {
            throw new EximException(curl_errno($curl) . '-' . $count . '-' . curl_error($curl) . $response);
        }

        $responseStatus = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        
        if ($responseStatus >= 500) {
            throw new EximException('Exim server error', $responseStatus);
        } else {
            $responseArray = json_decode($response, true);

            if ($responseStatus === 200) {
                return $responseArray;
            } else {
                throw new EximException(!empty($responseArray['message']) ? $responseArray['message'] : $response,
                    $responseStatus);
            }
        }

        return $response;
    }


    public function getLogs($params)
    {
        $response = $this->_performRequest('GET', '/get_log.php', null, $params);
        return $response;
    }


    public function getOpenLogs($params)
    {
        $response = $this->_performRequest('GET', '/get_open_log.php', null, $params);
        return $response;
    }


    public function getFrom()
    {
        return $this->from;
    }
}