<?php

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 22.08.2018
 * Time: 10:33
 */
namespace Newdir\Exim;

use Newdir\Exim\EximObject;

class EximList implements EximObject
{

    private $members = array();
    private $name;

    public function __construct($name, $members = null)
    {
        $this->members = $members;
        $this->name = $name;
    }


    public function addMember(EximMember $member)
    {
        $this->members[] = $member;
    }


    public function getFromFile($file_path)
    {
        $data = file_get_contents($file_path);
        $lines = explode("\n", $data);

        foreach ($lines as $line) {

            $attribs = explode(';', $line);

            $address = $attribs[0];
            $name = isset($attribs[1]) ? $attribs[1] : null;
            $id = isset($attribs[2]) ? $attribs[2] : null;

            $member = new EximMember($id, $address, $name);

            $this->members[] = $member;
        }

        return $this;
    }


    public function getPostData()
    {
        $data = [
            'name' => $this->name,
            'members' => []
        ];

        foreach ($this->members as $member) {
            $data['members'][] = $member->getPostData();
        }

        return $data;
    }

    public function isEmpty()
    {
        return empty($this->members);
    }
}