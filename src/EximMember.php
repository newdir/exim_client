<?php

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 22.08.2018
 * Time: 10:35
 */
namespace Newdir\Exim;


use Newdir\Exim\EximObject;


class EximMember implements EximObject
{

    private $address;
    private $name;
    private $id;

    public function __construct($id, $address, $name = null)
    {
        $this->name = $name;
        $this->id = $id;
        $this->address = $address;
    }


    public function getPostData()
    {
        $data = [
            'address' => $this->address,
            'id' => $this->id,
        ];

        if (!empty($this->_name)) {
            $data['name'] = $this->name;
        }

        return $data;
    }
}