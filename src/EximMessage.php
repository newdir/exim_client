<?php

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 22.08.2018
 * Time: 10:36
 */
namespace Newdir\Exim;


use Newdir\Exim\EximObject;


class EximMessage implements EximObject
{
    //Поля письма согласно RFC-822
    private $message_id;        //Уникальный идентификатор письма (при отправке с exim-сервера к нему будет добавлен id получателя)
    private $from;              //The From name of the message.
    private $to = array();      //The array of 'to' names and addresses.
    private $bcc = array();     //Получатель (получатели), невидимый для остальных получателей, если требуется. То есть те, кто перечислен в "To:" и "Cc:", не будут знать, что копия письма отправлена
    private $replyTo = array();
    private $subject;

    //Поля данных
    private $html;                  //Тело письма
    private $attachments = array(); //TODO седалаем попозже

    //Дополнительные атрибуты
    private $tags = array();        //Присваиваются письму, но не участвуют в отправке, нужны для дальнейшего анализа
    private $enableTestMode;        //Активирует режим тестирования (Определим его попозже)

    private $php_mailer_params = array();
    protected $type = 'message';

    protected $_api;


    public function __construct(EximApi $api)
    {
        $this->_api = $api;
        $this->from = $api->getFrom();
        //$this->enableTestMode = $api->getIsTestModeEnabled();
        $this->message_id = md5(time() . get_class($this));
    }

    public function getPostData()
    {
        $data = [
            'from' => $this->from,
            'to' => $this->to,
            'subject' => $this->getSubject(),
            'message_id' => $this->message_id,
            'type' => $this->type,
        ];

        if (!empty($this->bcc)) {
            $data['bcc'] = $this->formatAddresses($this->bcc);
        }
        if (!empty($this->replyTo)) {
            $data['h:Reply-To'] = $this->formatAddresses($this->replyTo);
        }

        if (!empty($this->html)) {
            $data['html'] = $this->html;
        }

        foreach ($this->attachments as $number => $attachment) {
            $data['attachment[' . ($number + 1) . ']'] = $this->attach($attachment);
        }

        if (!empty($this->enableTestMode)) {
            $data['o:testmode'] = 'yes';
        }

        return $data;
    }


    public function send()
    {
        return $this->_api->sendMessage($this);
    }


    public function getSubject()
    {
        return $this->subject;
    }


    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }


    public function setBody($html)
    {
        $this->html = $html;
        return $this;
    }


    public function setMessageId($message_id)
    {
        $this->message_id = $message_id;
        return $this;
    }


    public function setTo($address, $name = null)
    {
        $this->to = array($address, $name);
        return $this;
    }


    public function getId()
    {
        return $this->message_id;
    }


    private function formatAddress(array $addressData)
    {
        list($address, $name) = $addressData;
        if (is_null($name)) {
            return $address;
        }
        return $name . ' <' . $address . '>';
    }


    private function formatAddresses(array $addresses)
    {
        $formattedAddresses = array();
        foreach ($addresses as $address) {
            $formattedAddresses[] = $this->formatAddress($address);
        }
        return implode(', ', $formattedAddresses);
    }

    private function attach($attachment)
    {
        return new CURLFile($attachment);
    }

}